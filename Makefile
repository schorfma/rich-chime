generate-images: rich_chime/*.py
	rich-codex --use-pty --skip-git-checks --no-confirm --min-pct-diff 8 --terminal-width 80
