# 🛎️ Rich Chime

> Combination of notifications using _Python_ libraries [**rich**](https://pypi.org/project/rich/) (visual) and [**chime**](https://pypi.org/project/chime/) (audio) available as _Python_ library and _CLI_ command

<kbd>[Usage](#usage)</kbd> | <kbd>[Installation](#installation)</kbd> | <kbd>[Source Code](https://gitlab.com/schorfma/rich-chime)</kbd> | <kbd>[License: _MIT_](https://gitlab.com/schorfma/rich-chime/-/blob/main/LICENSE.md)</kbd>

## Usage

<details>
<summary>

Command Help `rich-chime --help`

</summary>

![`rich-chime --help`](./images/rich-chime_help.svg)

</details>

### Examples

#### Success

![`echo "Success Message" | rich-chime --success`](images/rich-chime_success.svg)

#### Warning

![`echo "Warning Message" | rich-chime --warning`](images/rich-chime_warning.svg)

#### Error

![`echo "Error Message" | rich-chime --error`](images/rich-chime_error.svg)

#### Info

![`echo "Info Message" | rich-chime --info`](images/rich-chime_info.svg)

## Installation

* `rich-chime` requires _Python `>=3.9, <3.12`_
* Installation via [`pipx`](https://pypa.github.io/pipx/) is recommended for _CLI_ command usage

```sh
pipx install git+https://gitlab.com/schorfma/rich-chime.git
```

### Installation via `pipx` from local repository

```sh
git clone git+https://gitlab.com/schorfma/rich-chime.git
pipx install rich-chime
```

### Installation via `pip`

```sh
pip install git+https://gitlab.com/schorfma/rich-chime.git
```

### Installation via `poetry` (Development)

```sh
git clone git+https://gitlab.com/schorfma/rich-chime.git
cd rich-chime
poetry install
```
