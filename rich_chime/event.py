from datetime import datetime
from typing import Literal, NamedTuple, Text

import chime
from rich import print
from rich.console import RenderableType
from rich.panel import Panel

from .theme import COLORS, EMOJIS


class RichChimeEvent(NamedTuple):
    message: Text
    event_level: Literal["success", "warning", "error", "info"] = "success"

    def chime(self) -> None:
        chime.notify(self.event_level, sync=False, raise_error=False)

    def __rich__(self) -> RenderableType:
        return Panel(
            self.message,
            title=(
                f" [{COLORS[self.event_level]} bold]"
                f"{EMOJIS[self.event_level]} - "
                f"{self.event_level.upper()}[/] "
            ),
            title_align="left",
            subtitle=datetime.now().isoformat(timespec="seconds", sep=" "),
            subtitle_align="right",
            border_style=COLORS[self.event_level],
        )

    def __call__(self, output: bool = True, audio: bool = True) -> None:
        if output:
            print(self)

        if audio:
            self.chime()


__all__ = ("RichChimeEvent",)
