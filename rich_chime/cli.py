import sys
from typing import Text

from typer import Typer

from .event import RichChimeEvent

RICH_CHIME_COMMAND = Typer()


@RICH_CHIME_COMMAND.command()
def rich_chime(
    message: Text = "-",
    level: Text = None,
    success: bool = False,
    warning: bool = False,
    error: bool = False,
    info: bool = False,
    output: bool = True,
    audio: bool = True,
) -> None:
    if message == "-":
        if sys.stdin.isatty():
            message = "[yellow]No message piped " "into [bold]rich-chime[/] command.[/]"
        else:
            message = sys.stdin.read()

    if not level:
        if success:
            level = "success"
        elif warning:
            level = "warning"
        elif error:
            level = "error"
        elif info:
            level = "info"
        else:
            level = "success"

    event = RichChimeEvent(message=message, event_level=level)
    event(output=output, audio=audio)


__all__ = ("rich_chime",)

if __name__ == "__main__":
    RICH_CHIME_COMMAND()
