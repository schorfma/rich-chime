COLORS = {
    "success": "green",
    "warning": "yellow",
    "error": "red",
    "info": "cyan",
}

EMOJIS = {
    "success": "✅",
    "warning": "⚠️",
    "error": "❌",
    "info": "ℹ️",
}

__all__ = ("COLORS", "EMOJIS")
